#include<stdio.h>

// Fonction bonjour
// Rôle : dit bonjour
// Paramètre d'entrée : 
//   prenom : prenom de la personne à saluer
// Valeur de retour : aucune
void bonjour(char *prenom)
{
    printf("Bonjour %s !\n", prenom);
    printf("Vous êtes désormais un as de Git, félicitations !\n");
}
